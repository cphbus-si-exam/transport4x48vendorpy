from datetime import datetime
import json

class transport:
    eventid = ''
    numberofperson = ''
    pickupdate =''
    pickuplocation =''
    dropofflocation =''
    type =''
    status = ''
    requesttype = ''

    def __init__(self,eventid, numberofperson, pickupdate, pickuplocation, dropofflocation, type, requesttype):
        self.eventid = eventid
        self.numberofperson =numberofperson
        self.pickupdate = pickupdate
        self.pickuplocation = pickuplocation
        self.dropofflocation = dropofflocation
        self.type = type
        self.requesttype = requesttype

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,sort_keys=True, indent=4)


    def get_eventid(self):
            return self.eventid

    def get_numberofperson(self):
        
        if self.numberofperson<0 or self.numberofperson>100:
            return False
        else:
            return self.numberofperson
            

    def get_pickupdate(self):
        
        if self.pickupdate < format(datetime.now()):            
            return False
        else:            
            return self.pickupdate

    def get_pickuplocation(self):
        
        if self.pickuplocation =='':
            return False
        else:
            return self.pickuplocation

    def get_dropofflocation(self):
       
        if self.dropofflocation == '':
            return False
        else:
            return self.dropofflocation

    def get_type(self):
        
        if self.type == '':
            return False
        else:
            return self.type

    def get_requesttype(self):
        return self.requesttype

    
                
