from flask import Flask, render_template, redirect, url_for, session, request, flash, jsonify, abort
import datetime
import json
import random
import requests
from transports import transport
import urllib.parse
import random

app = Flask(__name__)


eventid =''
numberofperson = ''
pickup =''
pickuplocation =''
dropofflocation =''
type =''


@app.route('/')
def home():    
	return 'Welcome to taxi 4x48'


@app.route('/taxa', methods=['POST','GET'])
def booktransport():	
	
	if request.method =='POST':		 		
			
		tempdata = request.data		
		newdata = tempdata.decode()		
		newdata= json.loads(tempdata)
		islist = isinstance(newdata, list)
				
		if len(newdata)==0:
			replypackage = {'eventid': '', 'description': 'The list is empty', 'status': 'false', 'additional_info': '', 'request_type': ''}
			print(replypackage)	
			return json.dumps(replypackage)
		
		elif islist!=True:
			
			eventid = newdata['eventid']
			requesttype = newdata['request_type']		

			info = 'Incoming data is not a list'
			status = 'false'
			addinfo =''
			replypackage = {'eventid': eventid, 'description': info, 'status': status, 'additional_info': addinfo, 'request_type': requesttype}
			print(replypackage)			
			
			return json.dumps(replypackage)
		else:		
		
			for order in newdata:
				
				eventid = order['eventid']
				numberofperson = order['numberofpersons']
				pickup = order['pickupdate']
				pickuplocation = order['pickuplocation']
				dropofflocation = order['dropofflocation']
				type = order['type']
				requesttype = order['request_type']		

				numberofperson = int(numberofperson)
				pickup = urllib.parse.unquote(pickup)
				
				#print(eventid, numberofperson, pickup, pickuplocation, dropofflocation, type)
				transobj = transport(eventid,numberofperson,pickup,pickuplocation,dropofflocation,type,requesttype)

				info = ''
				status = 'true'
				msg = []

				mess = ['travel easy with our service:your contact person is John','reach your destination fast: your contact person is Kurt','skip your timewaste:your contact person is speedy Gonzales']
				addinfo = random.choice(mess)

				if transobj.get_pickupdate()==False:
					msg.append("Cannot book prior to current date.")
					status = 'false'
					addinfo =''
				
				if transobj.get_numberofperson()==False:
					msg.append("Transportation limit is max 100 persons.")
					status = 'false'
					addinfo =''

				
				info = ' '.join(msg)		

				replypackage = {'eventid': eventid, 'description': info, 'status': status, 'additional_info': addinfo, 'request_type': requesttype}
				
			print(replypackage)			
				
			return json.dumps(replypackage)
		
	else:
			
		return "Welcome"
	
	return "Welcome to booking taxi"
	

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
	
